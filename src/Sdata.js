const Sdata = [
    { Id:1,
      imgsrc:
        "https://rukminim1.flixcart.com/image/150/150/k0jvy4w0/jewellery-set/m/y/z/n72684gldpht022018-trushi-original-imaewhndnmrzacxx.jpeg?q=70",
      titles: "Jwellery",
      sname: "Jwellery set , Bangles",
      link: "https://www.flipkart.com/?s_kwcid=AL!739!3!582822158567!e!!g!!flipkart&gclsrc=aw.ds&&semcmpid=sem_8024046704_brand_exe_buyers_goog&gclid=EAIaIQobChMIz8b65q-Q9gIVD9myCh0jdQ6LEAAYASAAEgKjQfD_BwE",
    },
    { Id : 2,
      imgsrc:
        "https://rukminim1.flixcart.com/image/150/150/k6mibgw0/dress/x/7/u/xs-sfdrss1792-sassafras-original-imafpyzrhmbmxtcr.jpeg?q=70",
      titles: "Dresses",
      sname: "Pink A-line Dress",
      link: "https://www.flipkart.com/?s_kwcid=AL!739!3!582822158567!e!!g!!flipkart&gclsrc=aw.ds&&semcmpid=sem_8024046704_brand_exe_buyers_goog&gclid=EAIaIQobChMIz8b65q-Q9gIVD9myCh0jdQ6LEAAYASAAEgKjQfD_BwE",
    },
    { Id:3,
      imgsrc:
        "https://rukminim1.flixcart.com/image/150/150/kflftzk0-0/sari/o/y/c/free-hitesh2series-fumvel-unstitched-original-imafwypg4fwky3a7.jpeg?q=70",
      titles: "Sarees",
      sname: "Black net saree",
      link: "https://meesho.com/dresses-women/pl/gvn1o",
    },
    { Id:4,
      imgsrc:
        "https://rukminim1.flixcart.com/image/150/150/kobspe80/fabric/3/u/o/yes-unstitched-tt-jdm-pink-1-2-kashvi-original-imag2sxrr9gagh3h.jpeg?q=70",
      titles: "Dress Materials",
      sname: "Suit Material",
      link: "https://meesho.com/dresses-women/pl/gvn1o",
    },
    {Id:5,
      imgsrc:
        "https://rukminim1.flixcart.com/image/200/200/kv8fbm80/sandal/m/b/d/4-7613026-bata-tan-original-imag86dfrjcszgbg.jpeg?q=70",
      titles: "Foot-Wear",
      sname: "Cat-walk heels",
      link: "https://www.myntra.com/dresses?f=Gender%3Amen%20women%2Cwomen&rf=Discount%20Range%3A50.0_100.0_50.0%20TO%20100.0",
    },
    { Id :6,
      imgsrc:
        "https://rukminim1.flixcart.com/image/200/200/jt4olu80/kurta/a/k/s/m-ss18kap016-orange-anmi-original-imafejhvztgs8gmh.jpeg?q=70",
      titles: "Kurti",
      sname: "Orange-Kurti",
      link: "https://www.myntra.com/dresses?f=Gender%3Amen%20women%2Cwomen&rf=Discount%20Range%3A50.0_100.0_50.0%20TO%20100.0",
    },
    { Id :7,
        imgsrc:
          "https://assets.myntassets.com/f_webp,dpr_1.0,q_60,w_210,c_limit,fl_progressive/assets/images/8376999/2019/1/16/ee7448f1-1915-46fe-9204-61bc75c7df531547635381768-Lino-Perros-Off-White-Quilted-Handheld-Bag-4051547635379760-1.jpg",
        titles: "Hand-bag",
        sname: "Handheld-bag",
        link: "https://www.myntra.com/dresses?f=Gender%3Amen%20women%2Cwomen&rf=Discount%20Range%3A50.0_100.0_50.0%20TO%20100.0",
      },
      { Id :8,
        imgsrc:
          "https://images.meesho.com/images/products/41496195/sncy3_512.jpg",
        titles: "Dress",
        sname: "Black-dress",
        link: "https://www.myntra.com/dresses?f=Gender%3Amen%20women%2Cwomen&rf=Discount%20Range%3A50.0_100.0_50.0%20TO%20100.0",
      },
  ];
  
    export default Sdata;
  