import React from "react";

function Card(props) {
  return (
    <div className="cards">
      <div className="card">
        <img src={props.imgsrc}/>
        <div className="card_info">
          <span className="Type">{props.titles} </span>
          <h4 className="card_title">{props.sname} </h4>
          <a href={props.link} target="blank">
            <button className="btn" type="submit">
              View
            </button>
          </a>
        </div>
      </div>
    </div>
  );
}

export default Card;
