import React, { useDebugValue } from 'react';
import ReactDOM from 'react-dom';
import Sdata from './Sdata';
import Card from './card'
import './index.css';
//import App from './App';
//import reportWebVitals from './reportWebVitals';

function ncard(val , index , arr ) {
  return (
    <Card 
    key={val.Id}
     imgsrc = {val.imgsrc}
    titles = {val.titles}
    sname = {val.sname}
    link={val.link}
    />
  );
}

ReactDOM.render
(<>{Sdata.map(ncard)}</>,
  document.getElementById('root')
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();
